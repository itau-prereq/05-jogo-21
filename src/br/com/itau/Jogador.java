package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogador {
    private List<Carta> mao;
    private String nome;
    private int ordem;
    private int pontos;

    public Jogador(List<Carta> mao, String nome, int ordem, int pontos) {
        this.mao = mao;
        this.nome = nome;
        this.ordem = ordem;
        this.pontos = pontos;
    }

    public List<Carta> getMao() {
        return mao;
    }

    public void setMao(List<Carta> mao) {
        this.mao = mao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getOrdem() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public void limpaMao(){
        this.mao.clear();
        this.pontos = 0;
    }

    public void addMao(Carta carta){
        this.mao.add(carta);
        this.pontos += carta.getValor();
    }

    public void imprimeMao(){
        for(Carta carta : this.mao){
            System.out.print(carta.toString() + " / ");
        }
        System.out.println(" ");
    }

    public String maoToString(){
        StringBuilder sb = new StringBuilder();

        sb.append("Mão { ");
        for(Carta carta : this.mao){
            sb.append(carta.toString() + " - ");
        }
        sb.append("}");

        return sb.toString();
    }
}
