package br.com.itau;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.shuffle;

public class Baralho {
    private List<Carta> cartas;

    public List<Carta> getMonte() {
        return cartas;
    }

    public void setMonte(List<Carta> cartas) {
        this.cartas = cartas;
    }

    public void iniciaBaralho() {
        List<Carta> cartas = new ArrayList<>();

        for(NaipeEnum naipe : NaipeEnum.values()){
            for (NumeroEnum numero : NumeroEnum.values()){
                cartas.add(new Carta(naipe, numero, numero.getValor()));
            }
        }

        shuffle(cartas);
        this.cartas = cartas;
    }

    public void imprimeBaralho() {
        List<Carta> cartas = new ArrayList<>();

        for(Carta carta : this.cartas){
            System.out.println(carta.toString());
        }
    }

    public Carta comprarCarta() {
        return this.cartas.remove(0);
    }

    public int getCartasRestantes() {
        return this.cartas.size();
    }
}
