package br.com.itau;

import java.util.List;

public class HistoricoJogo {
    private List<Jogador> jogadores;
    private Jogador vencedor;

    public HistoricoJogo(List<Jogador> jogadores, Jogador vencedor) {
        this.jogadores = jogadores;
        this.vencedor = vencedor;
    }

    public List<Jogador> getJogadores() {
        return jogadores;
    }

    public void setJogadores(List<Jogador> mao) {
        this.jogadores = mao;
    }

    public Jogador getVencedor() {
        return vencedor;
    }

    public void setVencedor(Jogador vencedor) {
        this.vencedor = vencedor;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Jogo { \n");
        for (Jogador j : jogadores){
            sb.append("Jogador " + j.getNome() + " { \n");
            sb.append(j.maoToString());
            sb.append("\nResultado: " + j.getPontos() + "}");
        }

        sb.append("\n\nVencedor: " + vencedor.getNome());

        return sb.toString();
    }
}
