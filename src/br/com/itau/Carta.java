package br.com.itau;

public class Carta {
    private NaipeEnum naipe;
    private NumeroEnum numero;
    private int valor;

    public Carta(NaipeEnum naipe, NumeroEnum numero, int valor) {
        this.naipe = naipe;
        this.numero = numero;
        this.valor = valor;
    }

    public NaipeEnum getNaipe() {
        return naipe;
    }

    public NumeroEnum getNumero() {
        return numero;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return this.valor;
    }

    @Override
    public String toString() {
        return numero + " de " + naipe;
    }
}
