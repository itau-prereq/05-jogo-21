package br.com.itau;

import jdk.nashorn.internal.scripts.JO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Jogo {
    private Baralho baralhoEmUso;
    private List<Jogador> jogadores;
    private List<HistoricoJogo> historico;

    public Jogo() {
        this.baralhoEmUso = new Baralho();
        this.jogadores = new ArrayList<>();
        this.historico = new ArrayList<>();
    }

    public void gravaHistorico(){
        historico.add(new HistoricoJogo(this.jogadores, getResultado()));
    }

    public Jogador getResultado(){

        Jogador vencedor = new Jogador(null, "Ninguem", 0, 0);

        for( Jogador j : this.jogadores){
            if(j.getPontos() <= 21){
                if(vencedor.getPontos() < j.getPontos()) {
                    vencedor = j;
                } else if (vencedor.getPontos() == j.getPontos()){
                    vencedor.setNome("Empate");
                }
            }
        }

        return vencedor;
    }

    public void comecarJogo(){
        this.baralhoEmUso = new Baralho();

        baralhoEmUso.iniciaBaralho();

        for(Jogador j : jogadores){
            j.limpaMao();

            j.addMao(baralhoEmUso.comprarCarta());
            j.addMao(baralhoEmUso.comprarCarta());
        }
    }

    public boolean terminarJogo(){
        gravaHistorico();

        IO io = new IO();
        io.imprimeResultado(this);

        return io.continuar().equals("s") ? true : false;
    }


    public void jogar(){
        IO io = new IO();

        for (Jogador player : jogadores){
            boolean fim = false;

            while(!fim){
                String acao = io.getAcao(player);
                if(acao.equals("comprar")){
                    player.addMao(this.baralhoEmUso.comprarCarta());
                } else
                if(acao.equals("parar")){
                    fim = true;
                }
            }
        }
    }

    public void addJogador(int nJogadores) throws Exception{
        IO io = new IO();
        if(nJogadores == 0){
            throw new Exception("Um jogo tem que ter um jogador no minimo");
        }
        for (int i = 1; i <= nJogadores; i++) {
            Map<String, String> dados = io.obterJogador();
            Jogador player1 = new Jogador(new ArrayList<>(), dados.get("nome"), i+1, 0);

            addJogador(player1);
        }
    }

    public List<Jogador> getJogadores() {
        return jogadores;
    }

    public Baralho getBaralhoEmUso() {
        return baralhoEmUso;
    }

    public void setBaralhoEmUso(Baralho baralhoEmUso) {
        this.baralhoEmUso = baralhoEmUso;
    }

    public void setJogadores(List<Jogador> jogadores) {
        this.jogadores = jogadores;
    }

    public List<HistoricoJogo> getHistorico() {
        return historico;
    }

    public void setHistorico(List<HistoricoJogo> historico) {
        this.historico = historico;
    }

    public void addJogador(Jogador jogador){
        this.jogadores.add(jogador);
    }

    public void imprimeHistorico(){
        IO io = new IO();
        io.imprimeHistorico(this);
    }
}
