package br.com.itau;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static void imprimeMsgInicial(){
        System.out.println("Bem vindo ao jogo(voce perdeu)");
    }

    public static String getAcao(Jogador jogador){
        Scanner scanner = new Scanner(System.in);

        System.out.println(jogador.getNome() + ", voce tem " + jogador.getPontos() + ". O que deseja fazer? (comprar/parar) ");

        return scanner.nextLine();
    }

    public static void imprimeMao(Jogador jogador){
        System.out.println("Sua mao: ");
        jogador.imprimeMao();
    }

    public static void imprimeResultado(Jogo jogo){
        for (Jogador j : jogo.getJogadores()){
            System.out.println("Mao do " + j.getNome() + ": ");
            j.imprimeMao();
        }

        System.out.println("Vencedor: " + jogo.getResultado().getNome());
    }

    public static int obterNumeroJogadores() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Numeros de jogadores: ");
        int numero = scanner.nextInt();

        return numero;
    }

    //DTO - data transfer object
    public static Map<String, String> obterJogador(){
        Scanner scanner = new Scanner(System.in);

        Map<String, String> jogador = new HashMap<>();

        System.out.println("Nome: ");
        String nome = scanner.nextLine();

        jogador.put("nome", nome);

        return jogador;
    }

    public static void imprimeErro(String msg){
        System.out.println(msg);
    }

    public String continuar() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Jogar novamente? (s/n) ");

        return scanner.nextLine();
    }

    public static void imprimeHistorico(Jogo jogo){
        for(HistoricoJogo h :  jogo.getHistorico()){
            System.out.println(h.toString());
        }
    }

}
