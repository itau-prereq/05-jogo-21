package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Jogo j = new Jogo();

        IO io = new IO();

        boolean jogoAtivo = true;
        io.imprimeMsgInicial();
        try{
            j.addJogador(io.obterNumeroJogadores());
        } catch (Exception e){
            io.imprimeErro(e.getMessage());
            jogoAtivo = false;
        }

        while(jogoAtivo) {
            j.comecarJogo();
            j.jogar();

            jogoAtivo = j.terminarJogo();
        }

        j.imprimeHistorico();
    }
}
